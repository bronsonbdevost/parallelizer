package parallelizer

type token struct{}

func Parallelize[T any, O any](data []T, function func(T) O, limit int) []O {
	sem := make(chan token, limit)
	var results []O

	// Spawn new goroutines, but wait when we are at the limit
	for _, i := range data {
		// Wait here until the sem has buffered space available
		sem <- token{}
		go func(i T) {
			results = append(results, function(i))
			// Free up a spot in the sem pool
			<-sem
		}(i)
	}

	// Drain the spawned pool
	for n := limit; n > 0; n-- {
		// Keep trying to fill up sem with tokens
		sem <- token{}
	}

	return results
}

func ParallelizeOrdered[T any, O any](data []T, function func(T) O, limit int) []O {
	sem := make(chan token, limit)
	orderRecord := make(map[int]O)

	// Spawn new goroutines, but wait when we are at the limit
	for idx, i := range data {
		// Wait here until the sem has buffered space available
		sem <- token{}
		go func(idx int, i T) {
			orderRecord[idx] = function(i)
			// Free up a spot in the sem pool
			<-sem
		}(idx, i)
	}

	// Drain the spawned pool
	for n := limit; n > 0; n-- {
		// Keep trying to fill up sem with tokens
		sem <- token{}
	}

	// Look for a faster way to fill up the slice
	results := make([]O, len(data))
	for idx, _ := range results {
		results[idx] = orderRecord[idx]
	}

	return results
}
