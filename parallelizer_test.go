package parallelizer

import (
	"testing"
	"time"
)

type Shim struct {
	seconds float32
	mult    int
}

func TestParallelizing(t *testing.T) {

	got := Parallelize([]float32{0.546, 0.7, 0.12, 0.32, 0.44, 0.985, 0.22, 0.05}, OperationWaitForIt, 3)
	want := []int{546, 700, 120, 320, 440, 985, 220, 50}

	if len(got) != len(want) {
		t.Errorf("got length of %d, wanted length of %d", len(got), len(want))
	}

	// Note, there is no way to compare elementwise because
	// Parallelize returns in no particular order
	for i := 0; i < len(want); i++ {
		if !contains(got, want[i]) {
			t.Errorf("wanted %d, not found in %#v", want[i], got)
		}
	}
}

func TestParallelizingOrdered(t *testing.T) {

	got := ParallelizeOrdered([]float32{0.546, 0.7, 0.12, 0.32, 0.44, 0.985, 0.22, 0.05}, OperationWaitForIt, 3)
	want := []int{546, 700, 120, 320, 440, 985, 220, 50}

	if len(got) != len(want) {
		t.Errorf("got length of %d, wanted length of %d", len(got), len(want))
	}

	for i := 0; i < len(want); i++ {
		if got[i] != want[i] {
			t.Errorf("got %d,wanted %d", want[i], got[i])
		}
	}
}

func TestParallelizingShim(t *testing.T) {

	got := Parallelize(
		[]Shim{
			{0.546, 2},
			{0.7, 2},
			{0.12, 2},
			{0.32, 2},
			{0.44, 2},
			{0.985, 2},
			{0.22, 3},
			{0.05, 2},
		},
		ShimFunction,
		3)
	want := []int{1092, 1400, 240, 640, 880, 1970, 660, 100}

	if len(got) != len(want) {
		t.Errorf("got length of %d, wanted length of %d", len(got), len(want))
	}

	// Note, there is no way to compare elementwise because
	// Parallelize returns in no particular order
	for i := 0; i < len(want); i++ {
		if !contains(got, want[i]) {
			t.Errorf("wanted %d, not found in %#v", want[i], got)
		}
	}
}

func TestParallelizingOrderedShim(t *testing.T) {

	got := ParallelizeOrdered(
		[]Shim{
			{0.546, 2},
			{0.7, 2},
			{0.12, 2},
			{0.32, 2},
			{0.44, 2},
			{0.985, 2},
			{0.22, 3},
			{0.05, 2},
		},
		ShimFunction,
		3)
	want := []int{1092, 1400, 240, 640, 880, 1970, 660, 100}

	if len(got) != len(want) {
		t.Errorf("got length of %d, wanted length of %d", len(got), len(want))
	}

	for i := 0; i < len(want); i++ {
		if got[i] != want[i] {
			t.Errorf("got %d,wanted %d", want[i], got[i])
		}
	}
}

func OperationWaitForIt(seconds float32) int {
	milliseconds := int(seconds * 1000)
	time.Sleep(time.Duration(milliseconds) * time.Millisecond)
	return milliseconds
}

func ShimFunction(input Shim) int {
	return OperationWaitForItShim(input.seconds, input.mult)
}

func OperationWaitForItShim(seconds float32, mult int) int {
	milliseconds := int(seconds * 1000)
	time.Sleep(time.Duration(milliseconds) * time.Millisecond)
	return milliseconds * mult
}

func contains[T comparable](s []T, str T) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}
